import React, {useState} from 'react';

import {TextField, Button} from '@material-ui/core';

function Customer(props) {
  const [pickupAddress, setPickupAddress] = useState('Paseo Destino');
  const [dropoffAddress, setDropoffAddress] = useState('Triangulo Las Animas');

  const submitBookingRequest = () => {
    fetch("http://localhost:3000/api/bookings", {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
          pickup_address: pickupAddress,
          dropoff_address: dropoffAddress,
          customer_id: props.username
        })
    })
    .then(res => res.json())
    .then(response => console.log(response));
  }

  const mystlye = {
    minWidth: "50%",
    minHeight: 50
  };

  return (
    <div>
      <div>
      <TextField
            id="pickup-address"
            value={pickupAddress}
            onChange={(ev) => setPickupAddress(ev.target.value)}
            label="Pickup address"
            variant="outlined"
            style={mystlye}
        />
      </div>
      <div>
      <TextField
            id="dropoff-address"
            value={dropoffAddress}
            onChange={(ev) => setDropoffAddress(ev.target.value)}
            label="Pickup address"
            variant="outlined"
            style={mystlye}
        />
      </div>
      <div>
      <Button
            id="submit-button"
            onClick={() => submitBookingRequest()}
            variant="outlined"
            size="large"
            color="primary"
            style={mystlye} >
        Submit request
        </Button>
      </div>
    </div>
  );
}

export default Customer;