var express = require('express');
var router = express.Router();
var pusher = require('../services/pusher_service');

router.post('/', function(req, res, next) {
  console.log(req.body);
  // System should:
  // - Find near-by taxis
  // - Select the one that is closest to the pickup address
  // - Contact such taxi, proposing him/her to take the ride
  // ----------------------------------
  // In this moment, the system always contacts "pedro"
  pusher.trigger('driver_pedro', 'booking_request', req.body);
  res.send({msg: 'We are processing your booking request'});
});

module.exports = router;