var express = require('express');
var logger = require('morgan');
var cors = require('cors');
 
var bookingsRouter = require('./routes/bookings');
 
var app = express();
app.use(cors());
 
app.use(logger('dev'));
app.use(express.json());
 
app.use('/api/bookings', bookingsRouter);
 
module.exports = app;