import Pusher from 'pusher-js';

var pusher = new Pusher('24d2f336edf8976705b7', {
    cluster: 'us2',
    forceTLS: true
});

export default pusher;